from google.appengine.api import users
from google.appengine.ext import db
import datetime

# User Guesses
class Entry(db.Model):
    user = db.UserProperty()
    match = db.IntegerProperty()
    home = db.StringProperty()
    away = db.StringProperty()
    date = db.DateTimeProperty(auto_now_add=True)

# Group Matches
class Game(db.Model):

    match = db.IntegerProperty()
    kickoff = db.DateTimeProperty()
    venue = db.StringProperty()
    group = db.StringProperty()
    home = db.StringProperty()
    score = db.StringProperty()
    away = db.StringProperty()

# latest betting 
class Betting(db.Model):
    match = db.IntegerProperty()
    home = db.StringProperty()
    draw = db.StringProperty()
    away = db.StringProperty()


# Say hello to the current user
user = users.get_current_user()

e1 = Entry(user=user, match=29, home="2", away="1")
e1.put()
e1 = Entry(user=user, match=43, home="2", away="1")
e1.put()
e1 = Entry(user=user, match=54, home="2", away="1")
e1.put()
e1 = Entry(user=user, match=58, home="2", away="1")
e1.put()
e1 = Entry(user=user, match=59, home="2", away="1")
e1.put()
e1 = Entry(user=user, match=60, home="2", away="1")
e1.put()
e1 = Entry(user=user, match=61, home="2", away="1")
e1.put()

e1 = Betting(match=29, home="22/10", draw="10", away="12")
e1.put()
e1 = Betting(match=43, home="22/10", draw="10", away="12")
e1.put()
e1 = Betting(match=54, home="22/10", draw="10", away="12")
e1.put()
e1 = Betting(match=58, home="22/10", draw="10", away="12")
e1.put()

#  Group A
s = Game(match = 1, kickoff = datetime.datetime.strptime('2010-06-11 15:00', '%Y-%m-%d %H:%M'), venue = 'Joburg Soccer City', group = 'A', home = 'South Africa', score = '1-1', away = 'Mexico')
s.put()
s = Game(match = 2, kickoff = datetime.datetime.strptime('2010-06-11 19:30', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = 'A', home = 'Uruguay', score = '0-0', away = 'France')
s.put()
s = Game(match = 17, kickoff = datetime.datetime.strptime('2010-06-16 19:30', '%Y-%m-%d %H:%M'), venue = 'Pretoria', group = 'A', home = 'South Africa', score = '0-3', away = 'Uruguay')
s.put()
s = Game(match = 18, kickoff = datetime.datetime.strptime('2010-06-17 19:30', '%Y-%m-%d %H:%M'), venue = 'Polokwane', group = 'A', home = 'France', score = '1-2', away = 'Mexico')
s.put()
s = Game(match = 33, kickoff = datetime.datetime.strptime('2010-06-22 15:00', '%Y-%m-%d %H:%M'), venue = 'Rustenburg', group = 'A', home = 'Mexico', score = '0-1', away = 'Uruguay')
s.put()
s = Game(match = 34, kickoff = datetime.datetime.strptime('2010-06-22 15:00', '%Y-%m-%d %H:%M'), venue = 'Bloemfontein', group = 'A', home = 'France', score = '1-2', away = 'South Africa')
s.put()
#  Group B
s = Game(match = 3, kickoff = datetime.datetime.strptime('2010-06-12 15:00', '%Y-%m-%d %H:%M'), venue = 'Joburg Ellis Park', group = 'B', home = 'Argentina', score = '1-0', away = 'Nigeria')
s.put()
s = Game(match = 4, kickoff = datetime.datetime.strptime('2010-06-12 12:30', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = 'B', home = 'South Korea', score = '2-0', away = 'Greece')
s.put()
s = Game(match = 19, kickoff = datetime.datetime.strptime('2010-06-17 15:00', '%Y-%m-%d %H:%M'), venue = 'Bloemfontein', group = 'B', home = 'Greece', score = '2-1', away = 'Nigeria')
s.put()
s = Game(match = 20, kickoff = datetime.datetime.strptime('2010-06-17 12:30', '%Y-%m-%d %H:%M'), venue = 'Joburg Soccer City', group = 'B', home = 'Argentina', score = '4-1', away = 'South Korea')
s.put()
s = Game(match = 35, kickoff = datetime.datetime.strptime('2010-06-22 19:30', '%Y-%m-%d %H:%M'), venue = 'Durban', group = 'B', home = 'Nigeria', score = '2-2', away = 'South Korea')
s.put()
s = Game(match = 36, kickoff = datetime.datetime.strptime('2010-06-22 19:30', '%Y-%m-%d %H:%M'), venue = 'Polokwane', group = 'B', home = 'Greece', score = '0-2', away = 'Argentina')
s.put()
#  Group C
s = Game(match = 5, kickoff = datetime.datetime.strptime('2010-06-12 19:30', '%Y-%m-%d %H:%M'), venue = 'Rustenburg', group = 'C', home = 'England', score = '1-1', away = 'USA')
s.put()
s = Game(match = 6, kickoff = datetime.datetime.strptime('2010-06-13 12:30', '%Y-%m-%d %H:%M'), venue = 'Polokwane', group = 'C', home = 'Algeria', score = '0-1', away = 'Slovenia')
s.put()
s = Game(match = 22, kickoff = datetime.datetime.strptime('2010-06-18 15:00', '%Y-%m-%d %H:%M'), venue = 'Joburg Ellis Park', group = 'C', home = 'Slovenia', score = '2-2', away = 'USA')
s.put()
s = Game(match = 23, kickoff = datetime.datetime.strptime('2010-06-18 19:30', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = 'C', home = 'England', score = '0-0', away = 'Algeria')
s.put()
s = Game(match = 37, kickoff = datetime.datetime.strptime('2010-06-23 15:00', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = 'C', home = 'Slovenia', score = '0-1', away = 'England')
s.put()
s = Game(match = 38, kickoff = datetime.datetime.strptime('2010-06-23 15:00', '%Y-%m-%d %H:%M'), venue = 'Pretoria', group = 'C', home = 'USA', score = '1-0', away = 'Algeria')
s.put()
#  Group D
s = Game(match = 7, kickoff = datetime.datetime.strptime('2010-06-13 19:30', '%Y-%m-%d %H:%M'), venue = 'Durban', group = 'D', home = 'Germany', score = '4-0', away = 'Australia')
s.put()
s = Game(match = 8, kickoff = datetime.datetime.strptime('2010-06-13 15:00', '%Y-%m-%d %H:%M'), venue = 'Pretoria', group = 'D', home = 'Serbia', score = '0-1', away = 'Ghana')
s.put()
s = Game(match = 21, kickoff = datetime.datetime.strptime('2010-06-18 12:30', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = 'D', home = 'Germany', score = '0-1', away = 'Serbia')
s.put()
s = Game(match = 24, kickoff = datetime.datetime.strptime('2010-06-19 15:00', '%Y-%m-%d %H:%M'), venue = 'Rustenburg', group = 'D', home = 'Ghana', score = '1-1', away = 'Australia')
s.put()
s = Game(match = 39, kickoff = datetime.datetime.strptime('2010-06-23 19:30', '%Y-%m-%d %H:%M'), venue = 'Joburg Soccer City', group = 'D', home = 'Ghana', score = '0-1', away = 'Germany')
s.put()
s = Game(match = 40, kickoff = datetime.datetime.strptime('2010-06-23 19:30', '%Y-%m-%d %H:%M'), venue = 'Nelspruit', group = 'D', home = 'Australia', score = '2-1', away = 'Serbia')
s.put()
#  Group E
s = Game(match = 9, kickoff = datetime.datetime.strptime('2010-06-14 12:30', '%Y-%m-%d %H:%M'), venue = 'Joburg Soccer City', group = 'E', home = 'Netherlands', score = 'v', away = 'Denmark')
s.put()
s = Game(match = 10, kickoff = datetime.datetime.strptime('2010-06-14 15:00', '%Y-%m-%d %H:%M'), venue = 'Bloemfontein', group = 'E', home = 'Japan', score = '2-1', away = 'Cameroon')
s.put()
s = Game(match = 25, kickoff = datetime.datetime.strptime('2010-06-19 12:30', '%Y-%m-%d %H:%M'), venue = 'Durban', group = 'E', home = 'Netherlands', score = '1-2', away = 'Japan')
s.put()
s = Game(match = 26, kickoff = datetime.datetime.strptime('2010-06-19 19:30', '%Y-%m-%d %H:%M'), venue = 'Pretoria', group = 'E', home = 'Cameroon', score = '2-0', away = 'Denmark')
s.put()
s = Game(match = 43, kickoff = datetime.datetime.strptime('2010-06-24 19:30', '%Y-%m-%d %H:%M'), venue = 'Rustenburg', group = 'E', home = 'Denmark', score = '3-1', away = 'Japan')
s.put()
s = Game(match = 44, kickoff = datetime.datetime.strptime('2010-06-24 19:30', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = 'E', home = 'Cameroon', score = '1-2', away = 'Netherlands')
s.put()
#  Group F
s = Game(match = 11, kickoff = datetime.datetime.strptime('2010-06-14 19:30', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = 'F', home = 'Italy', score = '2-1', away = 'Paraguay')
s.put()
s = Game(match = 12, kickoff = datetime.datetime.strptime('2010-06-15 12:30', '%Y-%m-%d %H:%M'), venue = 'Rustenburg', group = 'F', home = 'New Zealand', score = '1-0', away = 'Slovakia')
s.put()
s = Game(match = 27, kickoff = datetime.datetime.strptime('2010-06-20 12:30', '%Y-%m-%d %H:%M'), venue = 'Bloemfontein', group = 'F', home = 'Slovakia', score = 'v', away = 'Paraguay')
s.put()
s = Game(match = 28, kickoff = datetime.datetime.strptime('2010-06-20 15:00', '%Y-%m-%d %H:%M'), venue = 'Nelspruit', group = 'F', home = 'Italy', score = 'v', away = 'New Zealand')
s.put()
s = Game(match = 41, kickoff = datetime.datetime.strptime('2010-06-24 15:00', '%Y-%m-%d %H:%M'), venue = 'Joburg Ellis Park', group = 'F', home = 'Slovakia', score = 'v', away = 'Italy')
s.put()
s = Game(match = 42, kickoff = datetime.datetime.strptime('2010-06-24 15:00', '%Y-%m-%d %H:%M'), venue = 'Polokwane', group = 'F', home = 'Paraguay', score = 'v', away = 'New Zealand')
s.put()
#  Group G
s = Game(match = 13, kickoff = datetime.datetime.strptime('2010-06-15 15:00', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = 'G', home = 'Ivory Coast', score = 'v', away = 'Portugal')
s.put()
s = Game(match = 14, kickoff = datetime.datetime.strptime('2010-06-15 19:30', '%Y-%m-%d %H:%M'), venue = 'Joburg Ellis Park', group = 'G', home = 'Brazil', score = 'v', away = 'Korea DPR')
s.put()
s = Game(match = 29, kickoff = datetime.datetime.strptime('2010-06-20 19:30', '%Y-%m-%d %H:%M'), venue = 'Joburg Soccer City', group = 'G', home = 'Brazil', score = 'v', away = 'Ivory Coast')
s.put()
s = Game(match = 30, kickoff = datetime.datetime.strptime('2010-06-21 12:30', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = 'G', home = 'Portugal', score = 'v', away = 'Korea DPR')
s.put()
s = Game(match = 45, kickoff = datetime.datetime.strptime('2010-06-25 15:00', '%Y-%m-%d %H:%M'), venue = 'Durban', group = 'G', home = 'Portugal', score = 'v', away = 'Brazil')
s.put()
s = Game(match = 46, kickoff = datetime.datetime.strptime('2010-06-25 15:00', '%Y-%m-%d %H:%M'), venue = 'Nelspruit', group = 'G', home = 'Korea DPR', score = 'v', away = 'Ivory Coast')
s.put()
#  Group H
s = Game(match = 15, kickoff = datetime.datetime.strptime('2010-06-16 12:30', '%Y-%m-%d %H:%M'), venue = 'Nelspruit', group = 'H', home = 'Honduras', score = 'v', away = 'Chile')
s.put()
s = Game(match = 16, kickoff = datetime.datetime.strptime('2010-06-16 15:00', '%Y-%m-%d %H:%M'), venue = 'Durban', group = 'H', home = 'Spain', score = 'v', away = 'Switzerland')
s.put()
s = Game(match = 31, kickoff = datetime.datetime.strptime('2010-06-21 15:00', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = 'H', home = 'Chile', score = 'v', away = 'Switzerland')
s.put()
s = Game(match = 32, kickoff = datetime.datetime.strptime('2010-06-21 19:30', '%Y-%m-%d %H:%M'), venue = 'Joburg Ellis Park', group = 'H', home = 'Spain', score = 'v', away = 'Honduras')
s.put()
s = Game(match = 47, kickoff = datetime.datetime.strptime('2010-06-25 19:30', '%Y-%m-%d %H:%M'), venue = 'Pretoria', group = 'H', home = 'Chile', score = 'v', away = 'Spain')
s.put()
s = Game(match = 48, kickoff = datetime.datetime.strptime('2010-06-25 19:30', '%Y-%m-%d %H:%M'), venue = 'Bloemfontein', group = 'H', home = 'Switzerland', score = 'v', away = 'Honduras')
s.put()

# Last Sixteen
s = Game(match = 49, kickoff = datetime.datetime.strptime('2010-06-26 16:00', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = '16', home = 'Uruguay', score = 'v', away = 'South Korea')
s.put()
s = Game(match = 50, kickoff = datetime.datetime.strptime('2010-06-26 20:30', '%Y-%m-%d %H:%M'), venue = 'Rustenburg', group = '16', home = 'USA', score = 'v', away = 'Ghana')
s.put()
s = Game(match = 51, kickoff = datetime.datetime.strptime('2010-06-27 16:00', '%Y-%m-%d %H:%M'), venue = 'Bloemfontein', group = '16', home = 'Germany', score = 'v', away = 'England')
s.put()
s = Game(match = 52, kickoff = datetime.datetime.strptime('2010-06-27 20:30', '%Y-%m-%d %H:%M'), venue = 'Johannesburg', group = '16', home = 'Argentina', score = 'v', away = 'Mexico')
s.put()
s = Game(match = 53, kickoff = datetime.datetime.strptime('2010-06-28 16:00', '%Y-%m-%d %H:%M'), venue = 'Durban', group = '16', home = 'Netherlands', score = 'v', away = 'Slovakia')
s.put()
s = Game(match = 54, kickoff = datetime.datetime.strptime('2010-06-28 20:30', '%Y-%m-%d %H:%M'), venue = 'Johannesburg', group = '16', home = 'Brazil', score = 'v', away = 'Chile')
s.put()
s = Game(match = 55, kickoff = datetime.datetime.strptime('2010-06-29 16:00', '%Y-%m-%d %H:%M'), venue = 'Pretoria', group = '16', home = 'Paraguay', score = 'v', away = 'Japan')
s.put()
s = Game(match = 56, kickoff = datetime.datetime.strptime('2010-06-29 20:30', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = '16', home = 'Spain', score = 'v', away = 'Portugal')
s.put()

# Quarter Finals
s = Game(match = 57, kickoff = datetime.datetime.strptime('2010-07-02 14:00', '%Y-%m-%d %H:%M'), venue = 'Port Elizabeth', group = '8', home = 'Netherlands', score = 'v', away = 'Brazil')
s.put()
s = Game(match = 58, kickoff = datetime.datetime.strptime('2010-07-02 18:30', '%Y-%m-%d %H:%M'), venue = 'Johannesburg', group = '8', home = 'Uruguay', score = 'v', away = 'Ghana')
s.put()
s = Game(match = 59, kickoff = datetime.datetime.strptime('2010-07-03 14:00', '%Y-%m-%d %H:%M'), venue = 'Cape Town', group = '8', home = 'Argentina', score = 'v', away = 'Germany')
s.put()
s = Game(match = 60, kickoff = datetime.datetime.strptime('2010-07-03 18:30', '%Y-%m-%d %H:%M'), venue = 'Johannesburg', group = '8', home = 'Paraguay', score = 'v', away = 'Spain')
s.put()

for group in 'ABCDEFGH':
    url = "http://www.windrawwin.com/worldcupgroup.asp?l=WCG%s" % group
    page = urlfetch.fetch(url, headers = {'User-Agent': "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3"}) 
    html = page.content[12342:].replace('Holland','Netherlands') 
    soup = BeautifulSoup(html)

    print 'Group %s ' % group

    greentable = soup.findAll("table", "greentable")
    for gt in greentable:
        rows = gt.findAll("tr", "middletxt")
        for row in rows[1:]:
            cols = row.findAll("td")
            home, away, win, draw, lose = str(cols[2].a.contents[0]), str(cols[5].a.contents[0]), str(cols[0].a.contents[0]), str(cols[3].a.contents[0]), str(cols[6].a.contents[0])
            print home, away, win, draw, lose

            game = Game.gql("WHERE home = :1 and away = :2", home, away).get()
            if game:
                betting = Betting.gql("WHERE match = :1", game.match).get()
                if betting is None:                
                    betting = Betting(key_name='%s v %s' % (home, away), match=game.match, home=win, draw=draw, away=lose)
                else:
                    betting.home, betting.draw, betting.away = win, draw, lose
                betting.put()
                print '%s : %s %s - Draw - %s - %s %s ' % (game.match, game.home, home, draw, game.away, away)


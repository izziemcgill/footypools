import os
import string
import datetime

from django.utils import simplejson

from google.appengine.api import urlfetch
from google.appengine.ext import db
from google.appengine.ext.webapp import WSGIApplication, RequestHandler
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp import util
from google.appengine.api import users
from google.appengine.ext.webapp.util import login_required

from BeautifulSoup import BeautifulSoup
import feedparser
import re
import wsgiref.handlers

# Users
class Player(db.Model):
    user = db.UserProperty()
    nickname = db.StringProperty()
    league = db.StringProperty()
    wins = db.IntegerProperty()
    goals = db.IntegerProperty()
    points = db.IntegerProperty()

# Group Matches
class Game(db.Model):
    match = db.IntegerProperty()
    kickoff = db.DateTimeProperty()
    venue = db.StringProperty()
    group = db.StringProperty()
    home = db.StringProperty()
    score = db.StringProperty()
    away = db.StringProperty()

# latest betting 
class Betting(db.Model):
    match = db.IntegerProperty()
    home = db.StringProperty()
    draw = db.StringProperty()
    away = db.StringProperty()

# User Guesses
class Entry(db.Model):
    user = db.UserProperty()
    match = db.IntegerProperty()
    home = db.StringProperty()
    away = db.StringProperty()
    date = db.DateTimeProperty(auto_now_add=True)

# Front page
class MainPage(RequestHandler):

  def get(self):
      login = users.get_current_user()
      if login:
          values = {
            'user': login,
            'logout_url': users.create_logout_url(self.request.uri),
          }
      else:
          values = {
            'login_url': users.create_login_url(self.request.uri),
          }

      nickname = self.request.get('nickname')
      if nickname:
          user = Player.gql("WHERE nickname = :1", self.request.get('nickname')).get().user
          values.update({'nickname' : nickname})
      else:
          user = login

      games = Game.all()
      for game in games:
          i = game.match

          if user:
              entry = Entry.gql("WHERE match = :1 and user = :2", i, user).get()
              if entry is None:
                  home, away = '', ''
              else:
                  home, away = entry.home, entry.away
              
              if user == login and game.kickoff > datetime.datetime.now():
                  readonly = ""
              else:
                  readonly = "readonly='readonly'"
              
              odds = ['','','']
              betting = Betting.gql("WHERE match = :1", i).get()
              if betting:
                  odds = [betting.home, betting.draw, betting.away]
                  
              html =  "<input type='text' name='h%s' value='%s' size='2' %s />" % (i, home, readonly)
              html += "<input type='text' name='a%s' value='%s' size='2' %s />" % (i, away, readonly)

              if game.score != 'v':
                  html += '<span>(%s)</span>' % game.score
                  
          else:
              html =  "<span>%s</span>" % (game.score)
                  

          values.update({'game%s' % i: html})


      path = os.path.join(os.path.dirname(__file__), 'templates/base.html')
      self.response.out.write(template.render(path, values))

class UserPage(RequestHandler):

  @login_required
  def get(self):

      user = users.get_current_user()

      if user:
          values = {
            'user': user,
            'logout_url': users.create_logout_url(self.request.uri),
          }
      else:
          values = {
            'login_url': users.create_login_url(self.request.uri),
          }

      player = Player.get_by_key_name(user.email())
      if player is None:
          pass 
      else:
          values.update({'nickname' : player.nickname})
          values.update({'league' : player.league})

          games = Game.all().filter('score !=', 'v')
          players = Player.all().filter('league =', player.league).order('-points')

          for player in players:
              player.wins, player.goals, player.points = (0, 0, 0)

              for game in games:
                  i = game.match

                  entry = Entry.gql("WHERE match = :1 and user = :2", i, player.user).get()
                  if entry is not None:
                      guess = "%s-%s" % (entry.home, entry.away)
                      if guess == game.score:
                          player.wins += 1
                          player.points += 1

                      home, away = game.score.split('-')
                      if entry.home == entry.away and home == away:
                          player.wins += 1
                          player.points += 1

                      if entry.home > entry.away and home > away:
                          player.wins += 1
                          player.points += 1

                      if entry.home < entry.away and home < away:
                          player.wins += 1
                          player.points += 1
                          
                      if home == entry.home:
                          player.goals += 1
                          player.points += 1

                      if away == entry.away:
                          player.goals += 1
                          player.points += 1
                      
              player.put()

          players = Player.all().filter('league =', player.league).order('-points')
          values.update({'players' : players})

      path = os.path.join(os.path.dirname(__file__), 'templates/user.html')
      self.response.out.write(template.render(path, values))

  def post(self):
      
      user = users.get_current_user()
      player = Player.get_by_key_name(user.email())
      if player is None:
          player = Player(key_name=user.email())
          player.user = user

      nickname = self.request.get('nickname')
      player.league = self.request.get('league')      
      duplicate = Player.gql("WHERE nickname = :1", nickname).get()
      if duplicate is None:
          player.nickname = nickname
          player.put()
      else:
          if player.user != duplicate.user:
              self.response.out.write('Nickname nicked already')
          else:
              player.nickname = nickname
              player.put()
              self.redirect('/user/')
               
# score match 
class Vote(RequestHandler):

  def post(self, team, score):

    user = users.get_current_user()

    match = int(team[1:])+0
    e = Entry.gql("WHERE match = :1 and user = :2", match, user).get()
    if e is None:
        e = Entry(user=user, match=match)

    if team[0:1] == 'h':
        e.home = score
    else:
        e.away = score

    e.put()

class CronJob(RequestHandler):
    
    def get(self):

        response = urlfetch.fetch('http://www.scorespro.com/rss/live-soccer.xml')
        pattern = re.compile('.*  (.*)  - (.*)     (\d) - (\d) \(WORLD \(FIFA\) - Quarter Finals\).*')
        
        if response.status_code == 200:
            feed = feedparser.parse(response.content)
        
            for link in feed['entries']:
                result = link['title']
                if '(WORLD (FIFA) -' in result: 
                    f = pattern.match(result)
                    print '%s (%s-%s) %s' % (f.group(1),f.group(3),f.group(4),f.group(2))
                    games = Game.all().filter('home = ', f.group(1)).filter('away = ', f.group(2))
                    for game in games:
                        game.score = '%s-%s' % (f.group(3),f.group(4))
                        game.put()


class LatestBetting(RequestHandler):

  def get(self):
      
      url = "http://odds.football-data.co.uk/football/world-cup/"
      page = urlfetch.fetch(url, headers = {'User-Agent': "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3"}) 

      html = page.content 
      soup = BeautifulSoup(html)

      firstColumns = soup.findAll("td", "firstColumn")

      for firstColumn in firstColumns:
          cols = firstColumn.parent.findAll("td")
          team = firstColumn.a.string.split(' v ')
          home = cols[2].string.strip()
          draw = cols[3].string.strip()
          away = cols[4].string.strip()

          game = Game.gql("WHERE home = :1 and away = :2", team[0], team[1]).get()
          if game:
              betting = Betting.gql("WHERE match = :1", game.match).get()

              if betting is None:                
                  betting = Betting(key_name='%s v %s' % (team[0],team[1]), match=game.match, home=home, draw=draw, away=away)
    
              if home or away:
                  betting.home, betting.draw, betting.away = home, draw, away
                  betting.put()
                  self.response.out.write('%s : %s %s - Draw - %s - %s %s ' % (game.match, game.home, home, draw, game.away, away))
                
class ShowPage(RequestHandler):

  def get(self, page):

      user = users.get_current_user()
      if user:
          values = {
            'user': user,
            'logout_url': users.create_logout_url(self.request.uri),
          }
          player = Player.get_by_key_name(user.email())
          if player is None:
              pass 
          else:
              values.update({'nickname' : player.nickname})
              values.update({'league' : player.league})
      else:
          player = None
          values = {
            'login_url': users.create_login_url(self.request.uri),
          }

      if page in ('about', 'stats', 'bets',):

          overall, total = 0, 0
          home, away, draw = 0, 0, 0
          homes, aways, draws = {}, {}, {}
          games = Game.all()
          for game in games:
              i = game.match
              betting = Betting.gql("WHERE match = :1", i).get()
              if betting:
                  odds = [betting.home, betting.draw, betting.away]
              else:
                  odds = ['-','-','-']

              total = 0
              ws, ds, ls = '', '', ''
              try:
                  home_goals, away_goals = game.score.split('-')
                  if home_goals > away_goals:
                      ws = 'font-weight:bold'
                      home += 1
                      try:
                          homes[game.venue] += 1
                      except:
                          homes[game.venue] = 1                          
                  elif home_goals < away_goals:
                      ls = 'font-weight:bold;'
                      away += 1
                      try:
                          aways[game.venue] += 1
                      except:
                          aways[game.venue] = 1
                  else:
                      ds = 'font-weight:bold;'
                      draw += 1
                      try:
                          draws[game.venue] += 1
                      except:
                          draws[game.venue] = 1
                            
                  entry = Entry.gql("WHERE match = :1 and user = :2", i, player.user).get()
                  if entry and betting:                      
                      total = -1
                      if entry.home == entry.away:
                          if ds:
                              ds = 'font-weight:bold;color:green;'
                              total = pays(betting.draw)
                          else:
                              ds = 'color:red;'
                      elif entry.home > entry.away:
                          if ws:
                              ws = 'font-weight:bold;color:green;'
                              total = pays(betting.home)
                          else:
                              ws = 'color:red;'
                      elif entry.home < entry.away:
                          if ls:
                              ls = 'font-weight:bold;color:green;'
                              total = pays(betting.away)
                          else:
                              ls = 'color:red;'
              except:
                  pass

              html =  "<table><tr>"
              html += "<td style='%s'>%s</td><td style='%s'>%s</td><td style='%s'>%s</td><td>%s</td>" % (ws,odds[0],ds,odds[1],ls,odds[2],total)
              html += "</tr></table>"
                  
              overall += total
              values.update({'game%s' % i: html})                       

          values.update({'overall' : overall})                       
          values.update({'total' : total})                       
              
          values.update({'home' : home})
          values.update({'draw' : draw})
          values.update({'away' : away})

          values.update({'homes' : homes})
          values.update({'draws' : draws})
          values.update({'aways' : aways})


          path = os.path.join(os.path.dirname(__file__), 'templates/%s.html' % page)
          self.response.out.write(template.render(path, values))
      else:
          self.redirect("/")
                        
def pays(odds):
    
    try:
        dend, isor = odds.split('/')
        odds = float(dend) / float(isor)
    except:
        return round(eval(odds), 2)
    
    return round(odds,2)    
              
# Start listeners
def main():

  application = WSGIApplication([('/', MainPage), 
                                 ('/user/', UserPage), 
                                 ('/cronjob/', CronJob), 
                                 ('/betting/', LatestBetting), 
                                 ('/(.*)/(.*)/', Vote), 
                                 ('/(.*)/', ShowPage),], debug=True)

  wsgiref.handlers.CGIHandler().run(application)

if __name__ == "__main__":
    main()


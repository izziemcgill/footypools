from google.appengine.api import urlfetch
from google.appengine.ext import db
import feedparser, re

response = urlfetch.fetch('http://www.scorespro.com/rss/live-soccer.xml')
pattern = re.compile('FT  (.*)  - (.*)     (\d) - (\d) \(WORLD \(FIFA\) - .*')

if response.status_code == 200:
    feed = feedparser.parse(response.content)

    for link in feed['entries']:
        result = link['title']
        if '(WORLD (FIFA) - ' in result: 
            f = pattern.match(result)
            print '%s (%s-%s) %s' % (f.group(1),f.group(3),f.group(4),f.group(2))
            games = Game.all().filter('home = ', f.group(1)).filter('away = ', f.group(2))
            for game in games:
                game.score = '%s-%s' % (f.group(3),f.group(4))
                game.put()

